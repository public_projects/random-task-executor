:: cd D:\Users\sm13\Documents\development\ipf\be\branch_develop\device-analysis
:: CALL mvn clean install -DskipTests

cd D:\Users\sm13\Documents\development\ipf\be\branch_develop\ipf-gui-facade
CALL mvn clean install -DskipTests

timeout /t 2

cd D:\Users\sm13\Documents\development\ipf\be\branch_develop\ipf-graph-rendering
CALL mvn clean install -DskipTests

timeout /t 2

cd D:\Users\sm13\Documents\development\ipf\be\branch_develop\ipf-communication
CALL mvn clean install -DskipTests

timeout /t 2

cd D:\Users\sm13\Documents\development\ipf\be\branch_develop\emerald
CALL mvn clean install -DskipTests

timeout /t 2

cd D:\Users\sm13\Documents\development\random-task-executor\configured-tasks
start cmd /k call copy-emerald-config.bat
timeout /t 10

:: single MC
:: cd D:\Users\sm13\Documents\development\ipf\be\branch_develop\emerald\distribution\target\assembly\bin

:: multi-mc
cd D:\Users\sm13\Documents\development\ipf\be\branch_develop\emerald\distribution-multimc\target\assembly\bin

start cmd /k call karaf.bat clean debug
timeout /t 60
start wmplayer "D:\Users\sm13\Music\frm-phone\7 Years Old - By Lukas Graham (LYRICS).mp3"
pause