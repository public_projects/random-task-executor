package org.green.task.process;

public interface TaskProcess {
    String TO_LOCATIONS = "toLocations";
    String RAW_FILE_DIRECTORY = "rawFilesDirectory";
    String FROM_FILE = "file";
    String FROM_FILES = "files";
    String TO_LOCATION = "toLocation";
    String TARGET_DIRECTORY = "targetDirectory";
    String TEXT_TO_MATCH = "textToMatch";
    void execute();
}
