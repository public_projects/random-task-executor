package org.green.task.process.impl;

import ch.qos.logback.core.util.FileUtil;
import org.green.task.process.TaskProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CopyFilesToLocationsProcessor implements TaskProcess {
    private final static Logger LOGGER = LoggerFactory.getLogger(CopyFilesToLocationsProcessor.class);
    private final List<String> files;
    private final List<String> newLocations;

    public CopyFilesToLocationsProcessor(List<String> files, List<String> newLocations) {
        this.files = files;
        this.newLocations = newLocations;
    }

    @Override
    public void execute() {
        StringBuilder filesLog = new StringBuilder();
        StringBuilder LocationsLog = new StringBuilder();
        StringBuilder failed = new StringBuilder();
        filesLog.append(System.lineSeparator()).append("Files copied:").append(System.lineSeparator());
        LocationsLog.append(System.lineSeparator()).append("To locations:").append(System.lineSeparator());
        Set<String> toLocations = new HashSet<>();
        Set<String> frmFiles = new HashSet<>();
        Set<String> failedMessage = new HashSet<>();
        for (String src : files) {
            File srcFile = new File(src);
            if (!srcFile.isFile()) {
                failedMessage.add("Not a file: " + srcFile.getAbsolutePath());
                break;
            }

            for (String destination : newLocations) {
                File destinationFile = new File(destination);

                if (!destinationFile.isDirectory()) {
                    failedMessage.add("Not a directory: "+destinationFile.getAbsolutePath());
                    continue;
                }

                destinationFile = new File(destination + File.separator + srcFile.getName());
                try {
                    FileCopyUtils.copy(srcFile, destinationFile);
                    frmFiles.add(src);
                    toLocations.add(destination);
                }
                catch (IOException e) {
                    failed.append("Failed to copy: " + srcFile.getAbsolutePath()).append(System.lineSeparator())
                          .append(" to ").append(" directory: ")
                          .append(destinationFile.getAbsolutePath());
                    LOGGER.error("Failed while copying", e);
                }
            }
        }

        frmFiles.forEach(s -> {
            filesLog.append("   - ").append(s).append(System.lineSeparator());
        });

        toLocations.forEach(s -> {
            LocationsLog.append("   - ").append(s).append(System.lineSeparator());
        });

        failedMessage.forEach(s -> {
            failed.append(s).append(System.lineSeparator());
        });

        LOGGER.info(filesLog.append(LocationsLog).append(System.lineSeparator()).append(failed).toString());
    }
}
