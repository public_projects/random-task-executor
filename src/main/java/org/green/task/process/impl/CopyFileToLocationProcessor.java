package org.green.task.process.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.green.task.process.TaskProcess;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;


@AllArgsConstructor
@Slf4j
public class CopyFileToLocationProcessor implements TaskProcess {
    private String fromFile;
    private String toLocation;

    @Override
    public void execute() {
        log.info("Start ...");
        File src = new File(fromFile);
        log.info("Copiying file .. {}, fileName: {}", fromFile, src.getName());
        File destination = new File(toLocation+File.separator+src.getName());
        log.info("to location: {}", destination);

        try {
            FileCopyUtils.copy(src, destination);
        }
        catch (IOException e) {
            log.error("Failed while copying", e.getMessage());
        } finally {
            log.info("End ...");
        }
    }
}
