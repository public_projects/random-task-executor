package org.green.task.process.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.green.task.process.TaskProcess;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;


@AllArgsConstructor
@Slf4j
public class CopyFileToLocationsProcessor implements TaskProcess {

    private String id;
    private String fromFile;
    private List<String> toLocations;

    @Override
    public void execute() {
        StringBuilder logSummary = new StringBuilder();
        logSummary.append(System.lineSeparator());
        logSummary.append("Start ... processing [id: " + id + "]");
        logSummary.append(System.lineSeparator());
        File src = new File(fromFile);

        if (!src.exists()) {
            logSummary.append("FAIL: target file/folder not found!: [" + fromFile + "]")
                      .append(System.lineSeparator());
            logSummary.append("End\n\n");
            log.info("{}", logSummary.toString());
            return;
        }

        log.info("[{}] Copiying file .. {}, fileName: {}", id, fromFile, src.getName());
        logSummary.append("Copiying file .. " + fromFile + ", fileName: " + src.getName())
                  .append(System.lineSeparator());
        for (String location : toLocations) {
            String newFilePath = location + File.separator + src.getName();

            File destination = new File(newFilePath);
            String prefix = "PASS";

            try {
                FileCopyUtils.copy(src, destination);
            }
            catch (IOException e) {
//                log.error("Failed while copying", e);
                prefix = "FAIL";
            } finally {
                logSummary.append("- " + prefix + ": " + newFilePath)
                          .append(System.lineSeparator());
            }
        }
        logSummary.append("End\n\n");
        log.info("{}", logSummary.toString());
    }
}
