package org.green.task.process.impl;

import org.green.task.process.TaskProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class DeleteDirectoryWithCertainText implements TaskProcess {
    private final static Logger LOGGER = LoggerFactory.getLogger(DeleteDirectoryWithCertainText.class);
    private final String parentDirectory;
    private final List<String> textToMatch;

    public DeleteDirectoryWithCertainText(String parentDirectory, List<String> textToMatch) {
        this.parentDirectory = parentDirectory;
        this.textToMatch = textToMatch;
    }

    @Override
    public void execute() {
        File directory = new File(parentDirectory);

        if (!directory.isDirectory()) {
            LOGGER.error("This is not a directory: {} !", directory);
            return;
        }

        getSubdirs(directory).forEach(file -> {
            String fileName = file.getName().toLowerCase().trim();
            try {

            for (String text : textToMatch) {
                System.err.println("[" + fileName + "] contains " + "[" + text + "] == " + fileName.contains(text));
                if (fileName.contains(text)) {
                    LOGGER.info("directory: {} deleted!", file);
                        deleteDirectory(file);
                }
            }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }


    private List<File> getSubdirs(File file) {
        List<File> subdirs = Arrays.asList(file.listFiles(new FileFilter() {
            public boolean accept(File f) {
                return f.isDirectory();
            }
        }));

        subdirs = new ArrayList<>(subdirs);

        List<File> deepSubdirs = new ArrayList<File>();
        for (File subdir : subdirs) {
            deepSubdirs.addAll(getSubdirs(subdir));
        }
        subdirs.addAll(deepSubdirs);
        return subdirs;
    }

    void deleteDirectory(File file) throws IOException {
        if (file.isDirectory()) {
            File[] entries = file.listFiles();
            if (entries != null) {
                for (File entry : entries) {
                    deleteDirectory(entry);
                }
            }
        }
        if (!file.delete()) {
            throw new IOException("Failed to delete " + file);
        }
    }
}
