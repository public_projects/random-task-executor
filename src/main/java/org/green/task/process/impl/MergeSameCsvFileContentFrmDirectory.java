package org.green.task.process.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.green.task.process.TaskProcess;

import java.io.File;


@AllArgsConstructor
@Slf4j
public class MergeSameCsvFileContentFrmDirectory implements TaskProcess {

    private final String rawFileDirectory;

    @Override
    public void execute() {
        log.info("Start ...");
        if (!validate(rawFileDirectory)) return;

    }

    boolean validate(String directory) {
        File wDirectory = new File(directory);
        if (!wDirectory.isDirectory()) {
            log.error("Given {} is not a directory!", directory);
            return false;
        }

        isAllFilesSameType(wDirectory);
        return true;
    }


    boolean isAllFilesSameType(File directory) {
        return true;
    }
}
