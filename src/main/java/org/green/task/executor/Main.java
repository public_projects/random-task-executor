package org.green.task.executor;

import lombok.extern.slf4j.Slf4j;
import org.green.task.configuration.TaskRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class Main implements CommandLineRunner {
    @Autowired
    private TaskRegistry taskRegistry;

    @Override
    public void run(String... args) throws Exception {
        log.info("Getting all tasks ....");
        taskRegistry.getTasks().forEach(taskProcess -> taskProcess.execute());
    }
}
