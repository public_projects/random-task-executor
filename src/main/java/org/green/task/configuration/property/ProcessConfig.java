package org.green.task.configuration.property;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@ToString
@NoArgsConstructor
public class ProcessConfig {
    @Getter
    @Setter
    private String processType;
    private boolean enable = true;
    @Getter
    @Setter
    private List<Map<String, Object>> tasks = new ArrayList<>();

    public void setEnable(String enable){
        this.enable = new Boolean(enable);
    }

    public boolean isEnable(){
        return enable;
    }
}
