package org.green.task.configuration.property;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "task.config")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class TaskPropertyConfiguration {
    List<ProcessConfig> processConfigs = new ArrayList<>();
}
