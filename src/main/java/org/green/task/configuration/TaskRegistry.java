package org.green.task.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.green.task.configuration.property.ProcessConfig;
import org.green.task.configuration.property.TaskPropertyConfiguration;
import org.green.task.process.impl.*;
import org.green.task.process.TaskProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Component
@Slf4j
public class TaskRegistry {

    public static final String PROCESSOR_NAME = "processor";
    public static final String PROCESSOR_ID = "id";
    public static final String PROCESSOR_ENABLED = "enable";

    @Autowired
    private TaskPropertyConfiguration taskPropertyConfiguration;

    @Autowired
    private ObjectMapper objectMapper;

    private List<TaskProcess> processorList = new ArrayList<>();

    public List<TaskProcess> getTasks() {

        for (ProcessConfig processConfig : taskPropertyConfiguration.getProcessConfigs()) {
            log.info("processing: {}", processConfig.getProcessType());

            if (!processConfig.isEnable()) {
                log.info("processConfig: {} is disabled!", processConfig.getProcessType());
                continue;
            }

            for (Map<String, Object> task : processConfig.getTasks()) {
                String processorName = (String) task.get(PROCESSOR_NAME);
                Boolean isEnabled = (Boolean) task.get(PROCESSOR_ENABLED);
//                Boolean isEnabled = Boolean.parseBoolean(task.get(PROCESSOR_ENABLED) == null ? "true" : "false");
                log.info("processing id: {}", task.get(PROCESSOR_ID));
                if (isEnabled != null && !isEnabled) {
                    log.info("processConfig: {}, Process {} is not enabled!", processConfig.getProcessType(), processorName, isEnabled);
                    continue;
                } else log.info("processConfig: {}, Process {} is enabled!", processConfig.getProcessType(), processorName, isEnabled);

                if (isTypeOf(processorName, CopyFileToLocationProcessor.class)) {
                    log.info("Using processor {}", CopyFileToLocationProcessor.class.getSimpleName());
                    processorList.add(new CopyFileToLocationProcessor((String) task.get(TaskProcess.FROM_FILE),
                                                                      (String) task.get(TaskProcess.TO_LOCATION)));
                }
                else if (isTypeOf(processorName, MergeSameCsvFileContentFrmDirectory.class)) {
                    log.info("Using processor {}", MergeSameCsvFileContentFrmDirectory.class.getSimpleName());
                    processorList.add(new MergeSameCsvFileContentFrmDirectory((String) task.get(CopyFileToLocationProcessor.RAW_FILE_DIRECTORY)));
                }
                else if (isTypeOf(processorName, CopyFileToLocationsProcessor.class)) {
                    log.info("Using processor {}", CopyFileToLocationsProcessor.class.getSimpleName());

                    try {
                        Object data = task.get(CopyFileToLocationsProcessor.TO_LOCATIONS);
                        Map<String, String> toLocations = objectMapper.convertValue(data, Map.class);
                        List<String> values = new ArrayList<>(toLocations.values());
                        processorList.add(new CopyFileToLocationsProcessor((String) task.get(PROCESSOR_ID),
                                                                           (String) task.get(CopyFileToLocationProcessor.FROM_FILE),
                                                                           values));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                else if (isTypeOf(processorName, CopyFilesToLocationsProcessor.class)) {
                    List<String> filesToBeCopied = getList(TaskProcess.FROM_FILES, task, objectMapper);
                    List<String> locationsToBeCopied = getList(TaskProcess.TO_LOCATIONS, task, objectMapper);
                    processorList.add(new CopyFilesToLocationsProcessor(filesToBeCopied, locationsToBeCopied));
                }
                else if (isTypeOf(processorName, DeleteDirectoryWithCertainText.class)) {
                    String targetDirectory = (String) task.get(TaskProcess.TARGET_DIRECTORY);
                    List<String> textToWatch = getList(TaskProcess.TEXT_TO_MATCH, task, objectMapper);
                    processorList.add(new DeleteDirectoryWithCertainText(targetDirectory, textToWatch));
                }
                else {
                    log.info("Given processor {} not supported!", processorName);
                }
            }
        }
        return processorList;
    }

    public static boolean isTypeOf(String processName, Class cls) {
        return processName.equals(cls.getSimpleName());
    }

    public static List<String> getList(String paramName, Map<String, Object> objectMap, ObjectMapper mapper) {
        Object data = objectMap.get(paramName);
        Map<String, String> toLocations = mapper.convertValue(data, Map.class);
        return new ArrayList<>(toLocations.values());
    }
}
