package org.green.task;

import org.green.task.process.TaskProcess;
import org.green.task.process.impl.CopyFileToLocationProcessor;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

@SpringBootTest
class RandomTaskExecutorApplicationTests {
//	List<String> observationIds = new ArrayList<>();
//	Map<String, List<String>> observationIdentities = new HashMap<>();
//	Map<String, List<String>> observationIdentityTargets = new HashMap<>();
//
//	public void prepareData(){
//		observationIds.add("a");
//		observationIds.add("b");
//
//		List<String> observationIdentityA = new ArrayList<>(2);
//		observationIdentityA.add("1");
//		observationIdentityA.add("2");
//
//		List<String> observationIdentityB = new ArrayList<>(2);
//		observationIdentityB.add("3");
//		observationIdentityB.add("4");
//		observationIdentityB.add("5");
//
//		observationIdentities.putAll("a", observationIdentityA);
//		observationIdentities.putAll("b", observationIdentityB);
//
//		observationIdentityTargets.put("1", "a1-1");
//		observationIdentityTargets.put("1", "a1-2");
//		observationIdentityTargets.put("1", "a1-3");
//		observationIdentityTargets.put("2", "a2-2");
//	}

    @Test
    public void getDistance() {
        System.out.println(distance(32.9697, -96.80322, 29.46786, -98.53506, 'M') + " Miles\n");
        System.out.println(distance(32.9697, -96.80322, 29.46786, -98.53506, 'K') + " Kilometers\n");
        System.out.println(distance(4.995833, 100.418056, 4.994443, 100.418056, 'm') + " Meter\n");
        System.out.println(distance(4.995833, 100.418056, 4.994443, 100.418056, 'm') + " Meter\n");
        System.out.println(distance(43.503568, 16.476429, 43.506832, 16.476429, 'm') + " Meter\n");
        System.out.println(distance(32.9697, -96.80322, 29.46786, -98.53506, 'N') + " Nautical Miles\n");

        System.err.println("Test is type: " + isTypeOf("CopyFileToLocationProcessor", CopyFileToLocationProcessor.class));
    }

    private double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(
                deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == 'm') {
            dist = dist * 1.609344 * 1000;
        }
        else if (unit == 'N') {
            dist = dist * 0.8684;
        }
        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    public static boolean isTypeOf(String processName, Class cls) {
        return processName.equals(cls.getSimpleName());
    }

    @Test
    public void getFilesByName() {
        try (Stream<Path> stream = Files.find(Paths.get(
                "D:\\Users\\sm13\\Documents\\development\\ipf\\be\\branch_develop\\mcng-auditlog\\auditlog"),
                                              5,
                                              (path, attr) -> path.getFileName().toString().contains(".jar"))) {

            stream.filter(path -> !path.toFile().getName().contains("-sources.jar")).forEach(path -> {
                System.err.println();
                System.err.println(path.toAbsolutePath());
                String newPath = path.getParent().toAbsolutePath().toString() + File.separator + "classes";

                File newPathFile = new File(newPath);

                if (newPathFile.isDirectory()) {
                    newPath = newPath + File.separator + "META-INF";
                    newPathFile = new File(newPath);

                    if (newPathFile.isDirectory()) {
                        System.err.println(newPathFile.getAbsolutePath());
                        File manifestFile = new File(newPathFile + File.separator + "MANIFEST.MF");

                        if (manifestFile.isFile()) {
                            System.err.println("is a deployment jar!");

                            try {
                                Scanner myReader = new Scanner(manifestFile);
                                while (myReader.hasNextLine()) {
                                    String data = myReader.nextLine();

                                    if (data.contains("Bundle-SymbolicName: ")) {
//                                        System.out.println(data.replaceAll("\\.", "\\\\"));
                                        System.out.println(data.replaceAll("\\.", "\\\\"));
                                    } else {

                                    }
                                }
                                myReader.close();
                            } catch (FileNotFoundException e) {
                                System.out.println("An error occurred.");
                                e.printStackTrace();
                            }
                        }
                        else {
                            System.err.println("is test jar");
                        }
                    }
                    else {
                        System.err.println("is features file");
                    }
                }
                System.err.println(newPath);
                System.err.println(path.getFileName());
                System.err.println();
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSB(){
        StringBuilder sb = new StringBuilder();

        if (sb.toString().equals("")){
            System.err.println("isEmpty");
        }
    }
}
